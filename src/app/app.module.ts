import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MapaPage } from '../pages/mapa/mapa';
import { StorePage } from '../pages/store/store';
import { DrinkPage } from '../pages/drink/drink';
import { FoodPage } from '../pages/food/food';
import { ChatPage } from '../pages/chat/chat';
import { SecretPage } from '../pages/secret/secret';
import { PosterPage } from '../pages/poster/poster';

import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { AngularFireModule } from 'angularfire2';
import {AngularFireDatabase} from "angularfire2/database";
import {NativeStorage} from "@ionic-native/native-storage";
import {Network} from "@ionic-native/network";
import { Vibration } from '@ionic-native/vibration';
import {Push} from "@ionic-native/push";
import {ChatBubble} from "../components/chat-bubble/chat-bubble";
import {TrendsPage} from "../pages/trends/trends";
import { IonicStorageModule } from '@ionic/storage';


export const firebaseConfig = {
    apiKey: "AIzaSyABLymFo7-U2_ppwD0nBAqYUEQVC2HCSQM",
    authDomain: "tribaltech-enlighten.firebaseapp.com",
    databaseURL: "https://tribaltech-enlighten.firebaseio.com",
    projectId: "tribaltech-enlighten",
    storageBucket: "tribaltech-enlighten.appspot.com",
    messagingSenderId: "604298952208"
};
@NgModule({
    declarations: [
        MyApp,  HomePage,
        MapaPage,
        StorePage,
        DrinkPage,
        FoodPage,
        ChatPage,
        TrendsPage,
        //SecretPage,
        PosterPage,
        ChatBubble

    ],
    imports: [
        BrowserModule,
        IonicImageViewerModule,
        IonicModule.forRoot(MyApp, {
            backButtonText: '',
        }), IonicStorageModule.forRoot(),
        AngularFireModule.initializeApp(firebaseConfig)

    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        MapaPage,
        StorePage,
        DrinkPage,
        FoodPage,
        ChatPage,
        //SecretPage,
        PosterPage,
        TrendsPage,
        ChatBubble

    ],
    providers: [
        StatusBar,
        SplashScreen,
        NativePageTransitions,
        AngularFireDatabase,
        NativeStorage,
        Network,
        Push,
        Vibration,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {}
