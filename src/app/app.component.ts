import { Component, ViewChild } from '@angular/core';
import { AlertController, Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';

import { HomePage } from '../pages/home/home';
import { MapaPage } from '../pages/mapa/mapa';
import { StorePage } from '../pages/store/store';
import { DrinkPage } from '../pages/drink/drink';
import { FoodPage } from '../pages/food/food';
import { ChatPage } from '../pages/chat/chat';
import { AngularHelper } from "./helper/AngularHelper";
import { Network } from "@ionic-native/network";
import { NativeStorage } from "@ionic-native/native-storage";
import { AngularFireDatabase } from "angularfire2/database";
import { TrendsPage } from "../pages/trends/trends";
import { PosterPage } from "../pages/poster/poster";
import { Storage } from '@ionic/storage';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {

    @ViewChild(Nav) nav: Nav;

    rootPage: any = PosterPage;
    angularHelper: AngularHelper;

    pages: Array<{ title: string, component: any, porra: string }>;

    constructor(public storage: Storage, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public push: Push, public alertCtrl: AlertController, private af: AngularFireDatabase, private _nativeStorage: NativeStorage, private network: Network) {
        this.angularHelper = new AngularHelper(this.af, this._nativeStorage, this.network);

        this.initializeApp();

        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Lineup', component: HomePage, porra: 'assets/imgs/nosso-line.png' },
            { title: 'Mapa da Festa', component: MapaPage, porra: 'assets/imgs/mapa.png' },
            { title: 'Tribal Store', component: StorePage, porra: 'assets/imgs/store.png' },
            { title: 'Cardápio Drinks', component: DrinkPage, porra: 'assets/imgs/drink.png' },
            { title: 'Cardápio Foods', component: FoodPage, porra: 'assets/imgs/food.png' },

        ];
        this.angularHelper.chatIsActive((c) => {
            this.angularHelper.trendsIsActive((t) => {
                this.pages = [
                    { title: 'Lineup', component: HomePage, porra: 'assets/imgs/nosso-line.png' },
                    { title: 'Mapa da Festa', component: MapaPage, porra: 'assets/imgs/mapa.png' },
                    { title: 'Tribal Store', component: StorePage, porra: 'assets/imgs/store.png' },
                    { title: 'Cardápio Drinks', component: DrinkPage, porra: 'assets/imgs/drink.png' },
                    { title: 'Cardápio Foods', component: FoodPage, porra: 'assets/imgs/food.png' },
                ];
                if (t)
                    this.pages.push({ title: 'Trends', component: TrendsPage, porra: 'assets/imgs/trends.png' })
                if (c)
                    this.pages.push({ title: 'Chat', component: ChatPage, porra: 'assets/imgs/chat.png' })
            })
        })
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();

            this.pushsetup();

this.visualizouContrato()
            this._nativeStorage.getItem("idUser").then(
                (data) => {
                    console.log("logx:" + JSON.stringify(data))
                },
                error => {
                    this._nativeStorage.setItem("idUser", { id: this.angularHelper.makeid() })
                }
            );

        });
    }
    visualizouContrato() {
        this.storage.get("AceitouContrato")
            .then((res: any) => {
                if (res) {
                    this.rootPage = PosterPage
                }
                else{
                    this.nav.setRoot("ContratoPage")
                }
            })
    }
    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }
    pushsetup() {
        const options: PushOptions = {
            android: {
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };

        const pushObject: PushObject = this.push.init(options);

        pushObject.on('notification').subscribe((notification: any) => {
            if (notification.additionalData.foreground) {
                let youralert = this.alertCtrl.create({
                    title: 'New Push notification',
                    message: notification.message
                });
                youralert.present();
            }
        });

        pushObject.on('registration').subscribe((registration: any) => {
            //do whatever you want with the registration ID
        });

        pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error));
    }


}

