import {Component} from '@angular/core';

@Component({
    selector: 'chat-bubble',
    inputs: ['msg: message'],
    template:
        `
  <div class="chatBubble">
    <div class="chat-bubble {{msg.position}}">

      <div class="message">{{msg.content}}</div>
     
    </div>
  </div>
  `
})
export class ChatBubble {

}
