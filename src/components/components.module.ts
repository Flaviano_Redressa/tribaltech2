import { NgModule } from '@angular/core';
import { ChatBubble } from './chat-bubble/chat-bubble';
@NgModule({
	declarations: [ChatBubble],
	imports: [],
	exports: [ChatBubble]
})
export class ComponentsModule {}
