import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from "angularfire2/database";
import { NativeStorage } from "@ionic-native/native-storage";
import { Network } from '@ionic-native/network';
import { AngularHelper } from "../../app/helper/AngularHelper";
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SuperPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-super',
    templateUrl: 'super.html',
})
export class SuperPage {
    angularHelper: AngularHelper;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage: Storage,
        private af: AngularFireDatabase,
        private _nativeStorage: NativeStorage,
        private network: Network) {
        this.angularHelper = new AngularHelper(af, _nativeStorage, this.network);

    }
    ionViewWillEnter() {
        try {
            this.angularHelper.sinc(this.angularHelper.super1);
            setTimeout(() => {
                let a = this.angularHelper;
                this.storage.set("SuperPage", a)
            }, 3000);
        } catch (error) {
            this.storage.get("SuperPage").then(res => {
                if (res)
                    this.angularHelper = res;
            })
        }
    }

    gotoHome() {
        this.angularHelper.saveData(this.angularHelper.super1);
        // this.navCtrl.pop();
    }
    ionViewWillLeave() {
        this.gotoHome();
    }
}
