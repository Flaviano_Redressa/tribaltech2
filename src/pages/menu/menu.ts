import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import {NativeStorage} from "@ionic-native/native-storage";
import {Network} from "@ionic-native/network";
import {AngularHelper} from "../../app/helper/AngularHelper";
/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()

@Component({
    selector: 'page-menu',
    templateUrl: 'menu.html',
})
export class MenuPage {
    mainpages = [
        { component: "HomePage" },
        { component: "MapaPage" },
        { component: "StorePage" },
        { component: "DrinkPage" },
        { component: "FoodPage" },
        { component: "TrendsPage" },
    ];
    angularHelper : AngularHelper;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        private af: AngularFireDatabase,
        private _nativeStorage:NativeStorage,
        private network: Network) {
        console.log("logx: inicio");
        // Schedule delayed notification
        this.angularHelper = new AngularHelper(af,_nativeStorage,network);
    }


    ionViewDidLoad() {
        console.log('ionViewDidLoad MenuPage');

    }

    ionViewWillEnter(){
        console.log("logx:"+this.network.type);

        this.sinc();


    }

    gotoT(r){
        this.navCtrl.push(this.mainpages[r].component ,{},{animate:true,animation:'transition',duration:2000,direction:'forward'});
    }


    async sinc(){

        await  this.angularHelper.setData(this.angularHelper.burn);
        await  this.angularHelper.setData(this.angularHelper.organic);
        await  this.angularHelper.setData(this.angularHelper.super1);
        await  this.angularHelper.setData(this.angularHelper.progressive);
        await  this.angularHelper.setData(this.angularHelper.timetech);
        await  this.angularHelper.setData(this.angularHelper.tribaltech);
        await  this.angularHelper.setData(this.angularHelper.drink);
        await  this.angularHelper.setData(this.angularHelper.food);



//        console.log("logx:"+this.network.type)
//
//
// console.log("aqui1")
// setTimeout(async()=>{
//     this.burn= await this.af.list("/burn");
//     console.log("aqui1w")
//
//
//     let aux = new Array<{name:string,time:string}>();
//     await   this.burn.forEach(item =>{
//         aux.push(item)
//     })
//     this._nativeStorage.setItem("burn",aux).then(
//         () => {
//             console.log('logx:1 Stored item! ' + JSON.stringify(aux)) ;
//             //console.log("set:"+JSON.stringify(data));
//         },
//         error => {
//             console.error('logx:1 Error storing item'+JSON.stringify(error));
//         }
//     );
//     console.log("aqui14")
//
// },3000)
//
//
//
//            console.log("aqui16")
//
//             //-------------------------------------------------
//            this.organic= await this.af.list("/organic");
//
//            let aux1 = new Array<{name:string,time:string}>();
//          await   this.organic.forEach(item =>{
//                 aux1.push(item)
//             })
//             this._nativeStorage.setItem("organic",aux1).then(
//                 () => {
//                     console.log('logx: Stored item! ' + JSON.stringify(aux1)) ;
//                     //console.log("set:"+JSON.stringify(data));
//                 },
//                 error => {
//                     console.error('logx: Error storing item'+JSON.stringify(error));
//                 }
//             );;
//             //-------------------------------------------------
//            this.progressive= await this.af.list("/progressive");
//            console.log("aqui18")
//
//            let aux2 = new Array<{name:string,time:string}>();
//         await    this.progressive.forEach(item =>{
//                 aux2.push(item)
//             })
//             this._nativeStorage.setItem("progressive",aux2).then(
//                 () => {
//                     console.log('logx: Stored item! ' + JSON.stringify(aux2)) ;
//                     //console.log("set:"+JSON.stringify(data));
//                 },
//                 error => {
//                     console.error('logx: Error storing item'+JSON.stringify(error));
//                 }
//             );
//             //-------------------------------------------------
//            this.superi= await this.af.list("/super");
//
//            let aux3 = new Array<{name:string,time:string}>();
//             await    this.superi.forEach(item =>{
//                 aux3.push(item)
//             })
//             this._nativeStorage.setItem("super",aux3).then(
//                 () => {
//                     console.log('logx: Stored item! ' + JSON.stringify(aux3)) ;
//                     //console.log("set:"+JSON.stringify(data));
//                 },
//                 error => {
//                     console.error('logx: Error storing item'+JSON.stringify(error));
//                 }
//             );;
//             //-------------------------------------------------
//            this.timetech=await this.af.list("/timetech");
//
//            let aux4 = new Array<{name:string,time:string}>();
//             await      this.timetech.forEach(item =>{
//                 aux4.push(item)
//             })
//             this._nativeStorage.setItem("timetech",aux4).then(
//                 () => {
//                     console.log('logx: Stored item! ' + JSON.stringify(aux4)) ;
//                     //console.log("set:"+JSON.stringify(data));
//                 },
//                 error => {
//                     console.error('logx: Error storing item'+JSON.stringify(error));
//                 }
//             );;
//             //-------------------------------------------------
//            this.tribaltech = await this.af.list("/tribaltech");
//
//            let aux5 = new Array<{name:string,time:string}>();
//           await  this.tribaltech.forEach(item =>{
//                 aux5.push(item)
//             })
//             this._nativeStorage.setItem("tribaltech",aux5).then(
//                 () => {
//                     console.log('logx: Stored item! ' + JSON.stringify(aux5)) ;
//                     //console.log("set:"+JSON.stringify(data));
//                 },
//                 error => {
//                     console.error('logx: Error storing item'+JSON.stringify(error));
//                 }
//             );
//             //-------------------------------------------------
//             let aux6 = new Array<any>();
//
//
//          await   this.foods.forEach(item=>
//                 {
//                     item.forEach(i=>{
//                         aux6.push({n:i.$key,v:i})
//                     })
//
//                 }
//
//             );
//            this.foods = await this.af.list("/food");
//             this._nativeStorage.setItem("food",aux6).then(
//                 () => {
//                     console.log('logx: Stored item! ' + JSON.stringify(aux6)) ;
//                     //console.log("set:"+JSON.stringify(data));
//                 },
//                 error => {
//                     console.error('logx: Error storing item'+JSON.stringify(error));
//                 }
//             );
//
//             //-------------------------------------------------
//            this.drinks = await this.af.list("/drink");
//
//            let aux7 = new Array<any>();
//
//
//          await   this.drinks.forEach(item=>
//                 {
//                     item.forEach(i=>{
//                         aux7.push({n:i.$key,v:i})
//                     })
//
//                 }
//
//             )
//
//             this._nativeStorage.setItem("drink",aux7).then(
//                 () => {
//                     console.log('logx: Stored item! ' + JSON.stringify(aux7)) ;
//                     //console.log("set:"+JSON.stringify(data));
//                 },
//                 error => {
//                     console.error('logx: Error storing item'+JSON.stringify(error));
//                 }
//             );
//
//
//
//
//            console.log("aqui1656")
//


    }

}


