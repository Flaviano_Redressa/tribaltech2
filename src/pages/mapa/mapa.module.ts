import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapaPage } from './mapa';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { FileTransfer } from '@ionic-native/file-transfer';


@NgModule({
  declarations: [
    MapaPage,
  ],
  imports: [
    IonicImageViewerModule,
    IonicPageModule.forChild(MapaPage),
  ],
  providers:[
    FileTransfer
  ]
})
export class MapaPageModule {}
