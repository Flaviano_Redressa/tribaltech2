import { Component, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseApp } from 'angularfire2';
import * as firebase from 'firebase';
import { HomePage } from "../home/home";
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network';


@IonicPage()

@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {
  mapa = "";
  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public network: Network,
    public navParams: NavParams, @Inject(FirebaseApp) firebaseApp: firebase.app.App) {
    this.storage.get("MapaPage").then(res => {
      if (res)
        this.mapa = res;
    });
    const storageRef = firebaseApp.storage().ref().child('mapa.jpg');
    storageRef.getDownloadURL().then(url => {
      this.mapa = url;
      this.convertToDataURLviaCanvas(url, "image/jpeg").then(base64 => console.log(base64))
    }, (er => {
      this.storage.get("MapaPage").then(res => {
        this.mapa = res;
      })
    })
    )
  }
  convertToDataURLviaCanvas(url, outputFormat) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = () => {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'),
          dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        this.storage.set("MapaPage", dataURL)
        resolve(dataURL);
        canvas = null;
      };
      img.src = url;
    });
  }
  // downloadImage(url) {



  //     const fileTransfer: FileTransferObject  = this.transfer.create();


  //     fileTransfer.download(url,"").then((entry) => {
  //   alert(entry)


  //     }, (error) => {


  //     });

  //   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapaPage');
  }

  gotoHome() {
    this.navCtrl.setRoot(HomePage)
  }

}
