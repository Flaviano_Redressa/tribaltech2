import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyLineUpPage } from './my-line-up';

@NgModule({
    declarations: [
        MyLineUpPage,
    ],
    imports: [
        IonicPageModule.forChild(MyLineUpPage),
    ],
})
export class MyLineUpPageModule {}
