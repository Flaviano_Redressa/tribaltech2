import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import {NativeStorage} from "@ionic-native/native-storage";
import { Network } from '@ionic-native/network';
import set = Reflect.set;
import {AngularHelper} from "../../app/helper/AngularHelper";

/**
 * Generated class for the OrganicPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-organic',
  templateUrl: 'organic.html',
})
export class OrganicPage {

    angularHelper : AngularHelper;


    constructor(public navCtrl: NavController, public navParams: NavParams, private af: AngularFireDatabase,private _nativeStorage:NativeStorage,private network: Network) {
        this.angularHelper = new AngularHelper(af,_nativeStorage,this.network);

  }
    ionViewWillEnter(){
        this.angularHelper.sinc(this.angularHelper.organic);

    }


  ionViewDidLoad() {
    console.log('ionViewDidLoad OrganicPage');

  }

  gotoHome(){
      this.angularHelper.saveData(this.angularHelper.organic);

     // this.navCtrl.pop();
  }
    ionViewWillLeave(){
        this.gotoHome();
    }
}
