import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PosterPage } from './poster';

@NgModule({
  declarations: [
    PosterPage,
  ],
  imports: [
    IonicPageModule.forChild(PosterPage),
  ],
})
export class PosterPageModule {}
