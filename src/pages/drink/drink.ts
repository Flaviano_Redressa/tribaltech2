import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from "angularfire2/database";
import { NativeStorage } from "@ionic-native/native-storage";
import { Network } from "@ionic-native/network";
import { AngularHelper } from "../../app/helper/AngularHelper";
import { HomePage } from "../home/home";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
    selector: 'page-drink',
    templateUrl: 'drink.html',
})
export class DrinkPage {
    angularHelper: AngularHelper;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private af: AngularFireDatabase,
        public storage: Storage,
        private _nativeStorage: NativeStorage,
        private network: Network) {
        try {
            this.angularHelper = new AngularHelper(af, _nativeStorage, this.network);
            setTimeout(() => {
                let a = this.angularHelper.myData;
                this.storage.set("DrinkPage", a)
            }, 1000);
        } catch (error) {
            this.storage.get("DrinkPage").then(res => {
                if (res)
                    this.angularHelper.myData = res;
            })
        }
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DrinkPage');
        this.angularHelper.sinc(this.angularHelper.drink);


    }

    gotoHome() {
        this.navCtrl.setRoot(HomePage)
    }
    ionViewDidEnter() {

    }

}
