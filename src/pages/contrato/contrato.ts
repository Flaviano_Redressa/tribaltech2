import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PosterPage } from '../poster/poster';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ContratoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contrato',
  templateUrl: 'contrato.html',
})
export class ContratoPage {

  constructor(protected storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContratoPage');
  }
  closeModal() {
    this.storage.set("AceitouContrato", "Aceitou")
    this.navCtrl.setRoot(PosterPage);
  }
}
