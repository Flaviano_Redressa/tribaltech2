import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimetechPage } from './timetech';

@NgModule({
  declarations: [
    TimetechPage,
  ],
  imports: [
    IonicPageModule.forChild(TimetechPage),
  ],
})
export class TimetechPageModule {}
