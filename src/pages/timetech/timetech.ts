import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from "angularfire2/database";
import { NativeStorage } from "@ionic-native/native-storage";
import { Network } from '@ionic-native/network';
import { AngularHelper } from "../../app/helper/AngularHelper";
import { Storage } from '@ionic/storage';
/**
 * Generated class for the TimetechPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-timetech',
    templateUrl: 'timetech.html',
})
export class TimetechPage {
    angularHelper: AngularHelper;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public storage: Storage,
        private af: AngularFireDatabase,
        private _nativeStorage: NativeStorage,
        private network: Network) {
        this.angularHelper = new AngularHelper(af, _nativeStorage, this.network);


    }

    ionViewDidLoad() {
        try {
            this.angularHelper.sinc(this.angularHelper.timetech);
            setTimeout(() => {
                let a = this.angularHelper.myData;
                this.storage.set("TimetechPage", a)
            }, 1000);
        } catch (error) {
            this.storage.get("TimetechPage").then(res => {
                if (res)
                    this.angularHelper.myData = res;
            })
        }
    }

    gotoHome() {
        this.angularHelper.saveData(this.angularHelper.timetech);

        //this.navCtrl.pop();
    }
    ionViewWillLeave() {
        this.gotoHome();
    }
}
