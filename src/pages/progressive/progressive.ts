import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import {NativeStorage} from "@ionic-native/native-storage";
import { Network } from '@ionic-native/network';
import {AngularHelper} from "../../app/helper/AngularHelper";

/**
 * Generated class for the ProgressivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-progressive',
  templateUrl: 'progressive.html',
})
export class ProgressivePage {

    angularHelper : AngularHelper;

    constructor(public navCtrl: NavController, public navParams: NavParams, private af: AngularFireDatabase,private _nativeStorage:NativeStorage,private network: Network) {
        this.angularHelper = new AngularHelper(af,_nativeStorage,this.network);


  }
    ionViewDidEnter(){
        this.angularHelper.sinc(this.angularHelper.progressive);

    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProgressivePage');

  }

  gotoHome(){
      this.angularHelper.saveData(this.angularHelper.progressive);

    //  this.navCtrl.pop();
  }
    ionViewWillLeave(){
        this.gotoHome();
    }
}
