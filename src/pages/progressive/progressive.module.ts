import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgressivePage } from './progressive';

@NgModule({
  declarations: [
    ProgressivePage,
  ],
  imports: [
    IonicPageModule.forChild(ProgressivePage),
  ],
})
export class ProgressivePageModule {}
