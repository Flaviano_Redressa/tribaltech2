import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BurnPage } from './burn';

@NgModule({
  declarations: [
    BurnPage,
  ],
  imports: [
    IonicPageModule.forChild(BurnPage),
  ],
})
export class BurnPageModule {}
