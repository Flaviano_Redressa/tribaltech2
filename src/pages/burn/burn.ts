import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import {NativeStorage} from "@ionic-native/native-storage";
import { Network } from '@ionic-native/network';
import {AngularHelper} from "../../app/helper/AngularHelper";

/**
 * Generated class for the BurnPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-burn',
  templateUrl: 'burn.html',
})
export class BurnPage {


  angularHelper : AngularHelper;

  constructor(public navCtrl: NavController, public navParams: NavParams, private af: AngularFireDatabase,private _nativeStorage:NativeStorage,private network: Network) {
    this.angularHelper = new AngularHelper(af,_nativeStorage,this.network);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BurnPage');
    //this.sinc();
      this.angularHelper.sinc(this.angularHelper.burn);

  }
  ionViewDidEnter(){
     // this.sinc();
  }

  gotoHome(){
    this.angularHelper.saveData(this.angularHelper.burn);
  //  this.navCtrl.pop();
  }
    ionViewWillLeave(){
        this.gotoHome();
    }
}
