import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
//import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';


@IonicPage()


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pages = [
    { component: "TribaltechPage" },
    { component: "TimetechPage" },
    { component: "ProgressivePage" },
    { component: "OrganicPage" },
    { component: "BurnPage" },
    { component: "SuperPage" },
    { component: "MyLineUpPage"},
    { component: "SecretPage"}
  ];

  /*options: NativeTransitionOptions = {
    direction: 'left',
    duration: 15000,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 1500,
    androiddelay: 1500,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   };*/

  constructor(public navCtrl: NavController) {

  }

  goto(r){
    this.navCtrl.push(this.pages[r].component ,{},{animate:true,animation:'transition',duration:500,direction:'forward'});
    /*this.nativePageTransitions.slide(this.options);
    this.navCtrl.push(this.pages[r].component);*/
  }

  gotoHome(){
    this.navCtrl.pop();
  }

}
