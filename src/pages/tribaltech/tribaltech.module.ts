import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TribaltechPage } from './tribaltech';

@NgModule({
  declarations: [
    TribaltechPage,
  ],
  imports: [
    IonicPageModule.forChild(TribaltechPage),
  ],
})
export class TribaltechPageModule {}
