import {Component, ViewChild} from '@angular/core';
import {Events, IonicPage, NavController, NavParams, Content, Platform} from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import {NativeStorage} from "@ionic-native/native-storage";
import {Network} from "@ionic-native/network";
import {AngularHelper} from "../../app/helper/AngularHelper";

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
    @ViewChild(Content) content: Content;
    public msg = "";
    public chatBubble = new Array<{
        position: string,
        content: string,
        time: string,
    }>();
    angularHelper: AngularHelper;

    constructor(public navCtrl: NavController, public navParams: NavParams,private af: AngularFireDatabase,private _nativeStorage:NativeStorage,private network: Network) {
        this.angularHelper = new AngularHelper(this.af,this._nativeStorage,this.network);
        this.angularHelper.getChat(()=>{
            this.scroll()
        });


    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }
    carregaMsg(){

    }

    sendMsg(){

        if(this.msg.length != 0 ){
                //console.log("x:"+JSON.stringify(data));
            let day = new Date().getDate();
            let moth = new Date().getMonth()+1;
            let year = new Date().getFullYear();

            let hora = new Date().getHours();
            let minuto = new Date().getMinutes();

            let data = day + "/" + moth +"/"+year + " - " + hora+":" + minuto;


                this.angularHelper.sendChat(this.msg,data);
            this.msg="";

                //console.log(this.chats);
                setTimeout(()=>{
                    this.scroll();

                },500)


        }



    }
    scroll(){
        if(this.angularHelper.mensagem.length > 5){
            this.content.scrollToBottom(2);

        }
    }
}
