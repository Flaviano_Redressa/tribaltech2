import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from "angularfire2/database";
import { NativeStorage } from "@ionic-native/native-storage";
import { Network } from "@ionic-native/network";
import { AngularHelper } from "../../app/helper/AngularHelper";
import { HomePage } from "../home/home";
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-food',
  templateUrl: 'food.html',
})
export class FoodPage {
  angularHelper: AngularHelper;

  constructor(
    public storage: Storage,
    public navCtrl: NavController, public navParams: NavParams, private af: AngularFireDatabase, private _nativeStorage: NativeStorage, private network: Network) {
    try {
      this.angularHelper = new AngularHelper(af, _nativeStorage, this.network);
      setTimeout(() => {
        let a = this.angularHelper.myData;
        this.storage.set("FoodPage", a)
      }, 1000);
    } catch (error) {
      this.storage.get("FoodPage").then(res => {
        if (res)
          this.angularHelper.myData = res;
      })
    }
  }
  ionViewDidEnter() {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodPage');
    this.angularHelper.sinc(this.angularHelper.food);
  }

  gotoHome() {
    this.navCtrl.setRoot(HomePage)
  }

}
