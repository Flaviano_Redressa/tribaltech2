import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from "angularfire2/database";
import { NativeStorage } from "@ionic-native/native-storage";
import { Network } from "@ionic-native/network";
import { AngularHelper } from "../../app/helper/AngularHelper";
import { HomePage } from "../home/home";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-trends',
  templateUrl: 'trends.html',
})
export class TrendsPage {
  angularHelper: AngularHelper;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private af: AngularFireDatabase,
    public storage: Storage,
    private _nativeStorage: NativeStorage,
    private network: Network) {


    try {
      this.angularHelper = new AngularHelper(this.af, this._nativeStorage, this.network);
      this.angularHelper.getTrend();

      setTimeout(() => {
        let a = this.angularHelper.trends;
        if (a.length == 0) {
          this.storage.get("TrendsPage").then(res => {
            if (res)
              this.angularHelper.trends = res;
          })
        } else {
          this.storage.set("TrendsPage", a)
        }
      }, 1000);
    } catch (error) {
      this.storage.get("TrendsPage").then(res => {
        if (res)
          this.angularHelper.trends = res;
      })
    }


  }


  gotoHome() {
    this.navCtrl.setRoot(HomePage)
  }

}
