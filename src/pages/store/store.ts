import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireDatabase} from "angularfire2/database";
import {NativeStorage} from "@ionic-native/native-storage";
import { Network } from '@ionic-native/network';
import {AngularHelper} from "../../app/helper/AngularHelper";
import {HomePage} from "../home/home";

/**
 * Generated class for the StorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()

@Component({
  selector: 'page-store',
  templateUrl: 'store.html',
})
export class StorePage {
    angularHelper : AngularHelper;

    constructor(public navCtrl: NavController, public navParams: NavParams, private af: AngularFireDatabase,private _nativeStorage:NativeStorage,private network: Network) {
        this.angularHelper = new AngularHelper(af,_nativeStorage,this.network);
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StorePage');
      this.angularHelper.sinc(this.angularHelper.store);

  }

  gotoHome(){
      this.navCtrl.setRoot(HomePage)
  }

}
