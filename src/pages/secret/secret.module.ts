import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecretPage } from './secret';

@NgModule({
  declarations: [
   SecretPage,
  ],
  imports: [
    IonicPageModule.forChild(SecretPage),
  ],
})
export class SecretPageModule {}
