import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from "angularfire2/database";
import { NativeStorage } from "@ionic-native/native-storage";
import { Network } from '@ionic-native/network';
import { AngularHelper } from "../../app/helper/AngularHelper";
import { Storage } from '@ionic/storage';
/**
 * Generated class for the SuperPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-secret',
    templateUrl: 'secret.html',
})
export class SecretPage {
    angularHelper: AngularHelper;


    constructor(
        public navCtrl: NavController,
        public storage: Storage,
        public navParams: NavParams,
        private af: AngularFireDatabase,
        private _nativeStorage: NativeStorage,
        private network: Network) {
        this.angularHelper = new AngularHelper(af, _nativeStorage, this.network);

    }
    ionViewWillEnter() {
        try {
            this.angularHelper.sinc(this.angularHelper.secret);
            setTimeout(() => {
                let a = this.angularHelper.myData;
                this.storage.set("SecretPage", a)
            }, 1000);
        } catch (error) {
            this.storage.get("SecretPage").then(res => {
                if (res)
                    this.angularHelper.myData = res;
            })
        }

    }

    gotoHome() {
        this.angularHelper.saveData(this.angularHelper.secret);
        // this.navCtrl.pop();
    }
    ionViewWillLeave() {
        this.gotoHome();
    }
}
